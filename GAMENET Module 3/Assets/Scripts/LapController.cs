using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class LapController : MonoBehaviourPunCallbacks
{
    public List<GameObject> lapTriggers = new List<GameObject>();

	public enum RaiseEventCode
	{
		WhoFinishedEventCode = 0
	}

	private int finishOrder = 0;

	//OnEnable, OnDisable, & OnEvent - How you add listeners to all of the listeners for our events
	private void OnEnable()
	{
		PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
	}

	private void OnDisable()
	{
		PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
	}

	void OnEvent(EventData photonEvent)//adding a callback for whenever an event has been called
	{
		//checking if the event that has been sent was our WhoFinishedEvent, and if so- we need to retrieve the data that we have passed for our event
		if (photonEvent.Code == (byte)RaiseEventCode.WhoFinishedEventCode)
		{
			object[] data = (object[])photonEvent.CustomData;

			string nickNameOfFinishedPlayer = (string)data[0];
			finishOrder = (int)data[1];
			int viewId = (int)data[2];

			Debug.Log(nickNameOfFinishedPlayer + " " + finishOrder);

			GameObject orderUiText = RacingGameManager.instance.finisherTextsUI[finishOrder - 1];
			orderUiText.SetActive(true);

			if(viewId == photonView.ViewID)//checking if this is you
			{
				orderUiText.GetComponent<Text>().text = finishOrder + " " + nickNameOfFinishedPlayer + "(YOU)";
				orderUiText.GetComponent<Text>().color = Color.red;
			}
			else
			{
				orderUiText.GetComponent<Text>().text = finishOrder + " " + nickNameOfFinishedPlayer;
			}

		}
	}

    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject go in RacingGameManager.instance.lapTriggers)
		{
            lapTriggers.Add(go);
		}
    }

	private void OnTriggerEnter(Collider col)
	{
		if (lapTriggers.Contains(col.gameObject))
		{
			int indexOfTrigger = lapTriggers.IndexOf(col.gameObject);

			lapTriggers[indexOfTrigger].SetActive(false);
		}

		if (col.gameObject.tag == "FinishTrigger")
		{
			GameFinish();
		}
	}
	// Update is called once per frame
	void Update()
    {
        
    }

	public void GameFinish()
	{
		GetComponent<PlayerSetup>().camera.transform.parent = null;
		GetComponent<VehicleMovement>().enabled = false;

		finishOrder++;

		string nickname = photonView.Owner.NickName;
		int viewId = photonView.ViewID;

		//event data
		object[] data = new object[] {nickname, finishOrder, viewId/*for the purpose of seeing our ranking in the canvas*/};

		RaiseEventOptions raiseEventOptions = new RaiseEventOptions
		{
			Receivers = ReceiverGroup.All,
			CachingOption = EventCaching.AddToRoomCache
		};

		SendOptions sendOptions = new SendOptions
		{
			Reliability = false
		};
		PhotonNetwork.RaiseEvent((byte)RaiseEventCode.WhoFinishedEventCode, data/*The data we're passing for this specific event*/, raiseEventOptions, sendOptions);
	}
}
