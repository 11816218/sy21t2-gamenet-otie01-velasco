using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviourPunCallbacks
{
    public GameObject playerPrefab;

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
		{
            Respawn.instance.respawnPoints = GameObject.FindGameObjectsWithTag("respawn");
            int index = Random.Range(0, Respawn.instance.respawnPoints.Length);
            PhotonNetwork.Instantiate(playerPrefab.name, Respawn.instance.respawnPoints[index].transform.position, Quaternion.identity);
		}
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
