using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class NetworkManager : MonoBehaviourPunCallbacks
{
	[Header("Connection Status Panel")]
	public Text connectionStatusText;

    [Header("Login UI Panel")]
    public InputField playerNameInput;
	public GameObject loginUiPanel;

	[Header("Game Options Panel")]
	public GameObject gameOptionsPanel;

	[Header("Create Room Panel")]
	public GameObject createRoomPanel;
	public InputField roomNameInputField;
	public InputField playerCountInputField;

	[Header("Join Random Room Panel")]
	public GameObject joinRandomRoomPanel;

	[Header("Show Room List Panel")]
	public GameObject showRoomListPanel;

	[Header("Inside Room Panel")]
	public GameObject insideRoomPanel;
	public Text roomInfoText;
	public GameObject playerListItemPrefab;
	public GameObject playerListViewParent;
	public GameObject startGameButton;
	public GameObject leaveGameButton;

	[Header("Room List Panel")]
	public GameObject roomListPanel;
	public GameObject roomItemPrefab;
	public GameObject roomListParent;


	private Dictionary<string, RoomInfo> cachedRoomList;
	private Dictionary<string, GameObject> roomListGameObjects;
	private Dictionary<int, GameObject> playerListGameObjects;

	#region Unity Functions
	// Start is called before the first frame update
	void Start()
    {
		cachedRoomList = new Dictionary<string, RoomInfo>();
		roomListGameObjects = new Dictionary<string, GameObject>();
		ActivatePanel(loginUiPanel);

		PhotonNetwork.AutomaticallySyncScene = true;
    }

    // Update is called once per frame
    void Update()
    {
		connectionStatusText.text = "Connection Status: " + PhotonNetwork.NetworkClientState;
    }

	#endregion

	#region UI Callbacks
    public void OnLoginButtonClicked()
	{
        string playerName = playerNameInput.text;

        if (string.IsNullOrEmpty(playerName))
		{
            Debug.Log("Player name is invalid!");
		}
        else
		{
            PhotonNetwork.LocalPlayer.NickName = playerName;
            PhotonNetwork.ConnectUsingSettings();
		}
	}
	public void OnCreateRoomButtonClicked()
	{
		string roomName = roomNameInputField.text;
		if (string.IsNullOrEmpty(roomName))
		{
			roomName = "Room " + Random.Range(1000, 10000);
		}
		RoomOptions roomOptions = new RoomOptions();
		roomOptions.MaxPlayers = (byte)/*casting the int into a byte*/int.Parse(playerCountInputField.text);

		//creating a room, not a lobby
		//for now, we're using the default lobby since we're not accomodating 1mil+ players
		PhotonNetwork.CreateRoom(roomName, roomOptions);
	}
	public void OnCancelButtonClicked()
	{
		ActivatePanel(gameOptionsPanel);
	}
	public void OnShowRoomListButtonClicked()
	{
		if (!PhotonNetwork.InLobby)
		{
			PhotonNetwork.JoinLobby();
		}
		ActivatePanel(showRoomListPanel);
	}
	public void OnBackButtonClicked()
	{
		if (PhotonNetwork.InLobby)
		{
			PhotonNetwork.LeaveLobby();
		}
		ActivatePanel(gameOptionsPanel);
	}
	public void OnLeaveGameButtonClicked()
	{
		PhotonNetwork.LeaveRoom();//the callbacks such as the OnLeftRoom will be automatically called, and the code inside of it.
	}
	public void OnJoinRandomRoomClicked()
	{
		ActivatePanel(joinRandomRoomPanel);
		PhotonNetwork.JoinRandomRoom();
	}
	public void OnStartGameButtonClicked()
	{
		PhotonNetwork.LoadLevel("GameScene");
	}
	#endregion

	#region PUN Callbacks
	public override void OnConnected()
	{
		Debug.Log("Connected to the Internet");
	}
	public override void OnConnectedToMaster()
	{
		Debug.Log(PhotonNetwork.LocalPlayer.NickName + " has connected to the Photon Servers");
		ActivatePanel(gameOptionsPanel);
	}
	public override void OnCreatedRoom()
	{
		Debug.Log(PhotonNetwork.CurrentRoom.Name + " created!");
	}
	public override void OnJoinedRoom()
	{
		Debug.Log(PhotonNetwork.LocalPlayer.NickName + " has joined " + PhotonNetwork.CurrentRoom.Name);
		ActivatePanel(insideRoomPanel);

		roomInfoText.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name + " Current Player Counter: " + PhotonNetwork.CurrentRoom.PlayerCount + "/" + PhotonNetwork.CurrentRoom.MaxPlayers;

		if (playerListGameObjects == null)
		{
			playerListGameObjects = new Dictionary<int, GameObject>();
		}

		foreach(Player player in PhotonNetwork.PlayerList)
		{
			GameObject playerItem = Instantiate(playerListItemPrefab);
			playerItem.transform.SetParent(playerListViewParent.transform);
			playerItem.transform.localScale = Vector3.one;

			playerItem.transform.Find("PlayerNameText").GetComponent<Text>().text = player.NickName;
			//checking if the player in the list is actually you, so we use actornumber
			playerItem.transform.Find("PlayerIndicator").gameObject.SetActive(player.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber);

			playerListGameObjects.Add(player.ActorNumber, playerItem);
		}
	}
	//whenever a room is updated in the lobby, this will be called and it will return a list of all the room information that's currently inside our lobby.
	public override void OnRoomListUpdate(List<RoomInfo> roomList)
	{
		//calling this method before updating all of the room list information
		ClearRoomListGameObjects();
		Debug.Log("OnRoomListUpdate called");

		startGameButton.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);

		foreach(RoomInfo info in roomList)
		{
			Debug.Log(info.Name);

			//checking and filtering of rooms
			if(!info.IsOpen || !info.IsVisible || info.RemovedFromList)
			{
				//checking if there's duplicates, and if so then we need to remove that list first so that we have the most updated room list info update.
				if (cachedRoomList.ContainsKey(info.Name))
				{
					cachedRoomList.Remove(info.Name);//removing the duplicated room based by its name
				}
			}
			else
			{
				//updating existing rooms info
				if (cachedRoomList.ContainsKey(info.Name))
				{
					cachedRoomList[info.Name] = info;
				}
				else
				{
					cachedRoomList.Add(info.Name, info);
					#region comment/given problem
					//using the Dictionary Object- we're adding the roomList information with the key as the info.name, and the value as the info.
					//A Problem: what if a certain room is already in the cachedRoomList dictionary, and then the OnRoomListUpdate is called again because another room was created. when displayed, the room that was originally added in the cachedRoomList WILL be duplicated. So we need to filter out what needs to be added here, or if a certain room has been updated- then it also needs to be cached in here.
					#endregion
				}
			}
		}

		//instantiating the room
		foreach(RoomInfo info in cachedRoomList.Values)
		{
			GameObject listItem = Instantiate(roomItemPrefab);
			listItem.transform.SetParent(roomListParent.transform);
			listItem.transform.localScale = Vector3.one;

			listItem.transform.Find("RoomNameText").GetComponent<Text>().text = info.Name;
			listItem.transform.Find("RoomPlayersText").GetComponent<Text>().text = "Player count: " + info.PlayerCount + "/" + info.MaxPlayers;
			listItem.transform.Find("JoinRoomButton").GetComponent<Button>().onClick.AddListener(() => OnJoinRoomClicked(info.Name));
			//caching the gameobjects inside of the room list.
			roomListGameObjects.Add(info.Name, listItem);
		}
	}
	public override void OnLeftLobby()
	{
		ClearRoomListGameObjects();
		cachedRoomList.Clear();
	}
	//the three functions below allows the updating of each player's room view with the most updated list of players and also when a player leaves the room
	public override void OnPlayerEnteredRoom(Player player)
	{
		roomInfoText.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name + " Current Player Counter: " + PhotonNetwork.CurrentRoom.PlayerCount + "/" + PhotonNetwork.CurrentRoom.MaxPlayers;

		GameObject playerItem = Instantiate(playerListItemPrefab);
		playerItem.transform.SetParent(playerListViewParent.transform);
		playerItem.transform.localScale = Vector3.one;

		playerItem.transform.Find("PlayerNameText").GetComponent<Text>().text = player.NickName;
		playerItem.transform.Find("PlayerIndicator").gameObject.SetActive(player.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber);

		playerListGameObjects.Add(player.ActorNumber, playerItem);
	}
	public override void OnPlayerLeftRoom(Player otherPlayer)
	{
		startGameButton.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);

		roomInfoText.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name + " Current Player Counter: " + PhotonNetwork.CurrentRoom.PlayerCount + "/" + PhotonNetwork.CurrentRoom.MaxPlayers;

		Destroy(playerListGameObjects[otherPlayer.ActorNumber]);
		playerListGameObjects.Remove(otherPlayer.ActorNumber);
	}
	public override void OnLeftRoom()
	{
		foreach(var gameObject in playerListGameObjects.Values)
		{
			Destroy(gameObject);
		}
		playerListGameObjects.Clear();
		playerListGameObjects = null;
		ActivatePanel(gameOptionsPanel);
	}
	public override void OnJoinRandomFailed(short returnCode, string message)
	{
		Debug.Log(message);

		string roomName = "Room " + Random.Range(1000, 10000);
		RoomOptions roomOptions = new RoomOptions();
		roomOptions.MaxPlayers = 20;

		PhotonNetwork.CreateRoom(roomName, roomOptions);
	}

	#endregion

	#region Private Methods
	private void OnJoinRoomClicked(string roomName)
	{
		if (PhotonNetwork.InLobby)
		{
			PhotonNetwork.LeaveLobby();
		}
		PhotonNetwork.JoinRoom(roomName);//if we join a room, we leave the lobby
	}
	private void ClearRoomListGameObjects()
	{
		foreach(var item in roomListGameObjects.Values)
		{
			Destroy(item);
		}
		roomListGameObjects.Clear();
	}
	#endregion

	#region Public Methods
	public void ActivatePanel(GameObject panelToBeActivated)//activate or deactivate panels we want/don't want to shown
	{
		loginUiPanel.SetActive(panelToBeActivated.Equals(loginUiPanel));
		gameOptionsPanel.SetActive(panelToBeActivated.Equals(gameOptionsPanel));
		createRoomPanel.SetActive(panelToBeActivated.Equals(createRoomPanel));
		joinRandomRoomPanel.SetActive(panelToBeActivated.Equals(joinRandomRoomPanel));
		showRoomListPanel.SetActive(panelToBeActivated.Equals(showRoomListPanel));
		insideRoomPanel.SetActive(panelToBeActivated.Equals(insideRoomPanel));
		roomListPanel.SetActive(panelToBeActivated.Equals(roomListPanel));
	}
	#endregion


}
