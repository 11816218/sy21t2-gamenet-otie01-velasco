using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;//handles rooms and lobbies

public class LaunchManager : MonoBehaviourPunCallbacks
{
    public GameObject EnterGamePanel;
    public GameObject ConnectionStatusPanel;
    public GameObject LobbyPanel;

	void Awake()
	{
        PhotonNetwork.AutomaticallySyncScene = true;
	}

	// Start is called before the first frame update
	void Start()
    {
        EnterGamePanel.SetActive(true);
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public override void OnConnectedToMaster()//callback for connecting to photon servers
	{
        Debug.Log(PhotonNetwork.NickName + " connected to photon servers");
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(true);
	}

	public override void OnConnected()//callback for connecting to the internet. This goes first
	{
        Debug.Log(PhotonNetwork.NickName + " connected to internet");
	}

	public override void OnJoinRandomFailed(short returnCode, string message)
	{
        Debug.LogWarning(message);
        CreateAndJoinRoom();
	}

	public void ConnectToPhotonServer()
	{
        if (!PhotonNetwork.IsConnected)
		{
            PhotonNetwork.ConnectUsingSettings();
            ConnectionStatusPanel.SetActive(true);
            EnterGamePanel.SetActive(false);
        }
	}

    public void JoinRandomRoom()
	{
        PhotonNetwork.JoinRandomRoom();
	}

    private void CreateAndJoinRoom()
	{
        string randomRoomName = "Room " + Random.Range(0, 10000);

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 20;

        PhotonNetwork.CreateRoom(randomRoomName, roomOptions);
	}

	public override void OnJoinedRoom()
	{
        Debug.Log(PhotonNetwork.NickName + " has entered " + PhotonNetwork.CurrentRoom.Name);
        PhotonNetwork.LoadLevel("GameScene");
	}

	public override void OnPlayerEnteredRoom(Player newPlayer)
	{
        Debug.Log(newPlayer.NickName + " has entered room " + PhotonNetwork.CurrentRoom.Name + ". Room has now " + PhotonNetwork.CurrentRoom.PlayerCount + " players.");
	}
}
