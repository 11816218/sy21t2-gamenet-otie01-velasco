using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;

    [SerializeField]
    TextMeshProUGUI playerNameText;

    private Shooting shooting;
    private GrapplingGun grapplingGun;
    private MovementController movementController;

    // Start is called before the first frame update
    void Start()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();
        shooting = this.GetComponent<Shooting>();
        movementController = this.GetComponent<MovementController>();
        grapplingGun = this.GetComponentInChildren<GrapplingGun>();
        playerNameText.text = photonView.Owner.NickName;

        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
		{
            shooting.enabled = false;
			GetComponent<MovementController>().enabled = photonView.IsMine;
			GetComponent<MovementController>().speed = 7.5f;
			GetComponent<MovementController>().jumpForce = 5f;
            GetComponent<WallRun>().enabled = photonView.IsMine;
			GetComponent<LapController>().enabled = photonView.IsMine;
            GetComponentInChildren<GrapplingGun>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
		}
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dm"))
		{
            GetComponent<LapController>().enabled = false;
            GetComponent<WallRun>().enabled = false;
            GetComponent<MovementController>().enabled = photonView.IsMine;
            GetComponent<Shooting>().enabled = photonView.IsMine;
            GetComponentInChildren<GrapplingGun>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
        }
    }
}
