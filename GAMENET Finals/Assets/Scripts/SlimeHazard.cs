using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeHazard : MonoBehaviour
{
	public void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.CompareTag("Player"))
		{
			other.gameObject.GetComponent<MovementController>().speed /= 2f;
		}
	}

	public void OnTriggerExit(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			other.gameObject.GetComponent<MovementController>().speed *= 2f;
		}
	}
}
