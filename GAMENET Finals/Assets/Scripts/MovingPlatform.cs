using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
	[SerializeField] GameObject[] waypoints;
	int currentWaypointIndex = 0;

	[SerializeField] float speed = 5f;

	// Update is called once per frame
	void FixedUpdate()
    {
        MovePlatform();
    }

    public void MovePlatform()
	{
		if (Vector3.Distance(transform.position, waypoints[currentWaypointIndex].transform.position) < .1f)
		{
			currentWaypointIndex++;
			if (currentWaypointIndex >= waypoints.Length)
			{
				currentWaypointIndex = 0;
			}
		}

		transform.position = Vector3.MoveTowards(transform.position, waypoints[currentWaypointIndex].transform.position, speed * Time.deltaTime);
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.transform.SetParent(transform);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.transform.SetParent(null);
        }
        else if (collision.gameObject.CompareTag("Player") && collision.gameObject.GetComponent<MovementController>().isGrounded == false)
		{
            collision.gameObject.transform.SetParent(null);
        }
    }

}



