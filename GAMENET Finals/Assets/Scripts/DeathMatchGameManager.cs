using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DeathMatchGameManager : MonoBehaviourPunCallbacks
{
    public GameObject playerPrefab;
    public static DeathMatchGameManager instance = null;

    public bool GameFinished;
    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            Respawn.instance.respawnPoints = GameObject.FindGameObjectsWithTag("respawn");
            int index = Random.Range(0, Respawn.instance.respawnPoints.Length);
            PhotonNetwork.Instantiate(playerPrefab.name, Respawn.instance.respawnPoints[index].transform.position, Quaternion.identity);
        }

    }
}
