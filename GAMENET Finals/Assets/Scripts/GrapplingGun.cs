using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingGun : MonoBehaviour
{
    private LineRenderer lineRenderer;
    private Vector3 grapplePoint; // where we grappled
    public LayerMask Grappable; // what can we grapple to
    [SerializeField] private Transform grappleTip;
    [SerializeField] private Transform camera;
    [SerializeField] private Transform playerTransform;
    [SerializeField] public float maxDistance = 25f;
    private SpringJoint joint;

    private void Awake()
    {
        lineRenderer = this.GetComponent<LineRenderer>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
		{
            StartGrapple();
		}
        else if (Input.GetKeyUp(KeyCode.E))
		{
            StopGrapple();
		}
    }

    private void FixedUpdate()
    {
        DrawLine();
    }

    public void StartGrapple()
    {
        RaycastHit hit;
        if (Physics.Raycast(camera.position, camera.forward, out hit, maxDistance, Grappable))
        {
            grapplePoint = hit.point;
            joint = playerTransform.gameObject.AddComponent<SpringJoint>();

            joint.autoConfigureConnectedAnchor = false;
            joint.connectedAnchor = grapplePoint;

            float distanceFromPoint = Vector3.Distance(playerTransform.position, grapplePoint);

            // the distance grapple will try to keep from grapple point
            // values here can be modified 
            joint.maxDistance = distanceFromPoint * 0.8f;
            joint.minDistance = distanceFromPoint * 0.25f;

            joint.spring = 4.5f;
            joint.damper = -1f;
            joint.massScale = 4.5f;

            lineRenderer.positionCount = 2;
        }
    }

    public void StopGrapple()
    {
        lineRenderer.positionCount = 0;
        Destroy(joint);
    }

    void DrawLine()
    {
        //no line drawn if there is no joint
        if (!joint) return;

        lineRenderer.SetPosition(0, grappleTip.position);
        lineRenderer.SetPosition(1, grapplePoint);
    }

    public bool isGrappling()
    {
        return joint != null;
    }

    public Vector3 GetGrapplePoint()
    {
        return grapplePoint;
    }
}

