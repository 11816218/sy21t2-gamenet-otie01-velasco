﻿using UnityEngine;
using Photon.Pun;
public class MovementController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    public float speed = 10f;
    public float jumpForce = 10f;
    public bool isGrounded;

    [SerializeField]
    private float lookSensitivity = 3f;

    [SerializeField]
    GameObject fpsCamera;

    private Vector3 velocity = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    private float CameraUpAndDownRotation = 0f;
    private float CurrentCameraUpAndDownRotation = 0f;

    [Header("Ground Detection")]
    [SerializeField] LayerMask GroundMask;
    [SerializeField] Transform GroundCheck;
    [SerializeField] float GroundDistance = 0.2f;

    private Rigidbody rb;
    // Start is called before the first frame update
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    private void Update()
    {
        //Calculate movement veloc,ty as a 3d vector
        float xMovement = Input.GetAxis("Horizontal");
        float zMovement = Input.GetAxis("Vertical");

        Vector3 _movementHorizontal = transform.right * xMovement;
        Vector3 _movementVertical = transform.forward * zMovement;

        //Final movement velocty vector
        Vector3 _movementVelocity = (_movementHorizontal + _movementVertical).normalized * speed;

        isGrounded = Physics.CheckSphere(GroundCheck.position, GroundDistance, GroundMask);//checks if groundcheck true, distance, and layer mask

        //Apply movement
        Move(_movementVelocity);
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded == true)
            Jump();

        //calculate rotation as a 3D vector for turning around.
        float _yRotation = Input.GetAxis("Mouse X");
        Vector3 _rotationVector = new Vector3(0,_yRotation,0)*lookSensitivity;


        //Apply rotation
        Rotate(_rotationVector);



        //Calculate look up and down camera rotation
        float _cameraUpDownRotation = Input.GetAxis("Mouse Y")*lookSensitivity;

        //Apply rotation
        RotateCamera(_cameraUpDownRotation);

        if (this.transform.position.y < -20)
		{
            photonView.RPC("Respawn", RpcTarget.AllBuffered);
		}
        else if (Input.GetKeyDown(KeyCode.Q))
		{
            photonView.RPC("Respawn", RpcTarget.AllBuffered);
        }
    }

    //runs per physics iteration
    private void FixedUpdate()
    {
        if (velocity!=Vector3.zero)
        {
            rb.MovePosition(rb.position+velocity*Time.fixedDeltaTime);


        }

        rb.MoveRotation(rb.rotation*Quaternion.Euler(rotation));



        if (fpsCamera!=null)
        {

            CurrentCameraUpAndDownRotation -= CameraUpAndDownRotation;
            CurrentCameraUpAndDownRotation = Mathf.Clamp(CurrentCameraUpAndDownRotation,-85,85);

            fpsCamera.transform.localEulerAngles = new Vector3(CurrentCameraUpAndDownRotation,0,0);
        }
    }

    void Move(Vector3 movementVelocity)
    {
        velocity = movementVelocity;
    }

    void Rotate(Vector3 rotationVector)
    {
        rotation = rotationVector;
    }

    void RotateCamera(float cameraUpAndDownRotation)
    {
        CameraUpAndDownRotation = cameraUpAndDownRotation;
    }

    void Jump()
	{
        rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
        rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
    }


    [PunRPC]
    private void Respawn()
    {
        this.transform.parent = null;
        if (CheckpointSystem.instance)
        {
            CheckpointSystem.instance.RespawnPlayer(transform);
            
        }
    }
}
