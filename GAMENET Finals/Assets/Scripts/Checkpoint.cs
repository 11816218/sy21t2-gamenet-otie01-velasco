using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Checkpoint : MonoBehaviour
{
	private enum CoordinateSource
	{
		TransformPosition,
		ColliderPosition,
		Vector3Variable
	}

	[SerializeField] private CoordinateSource coordinateSource;
	[SerializeField] private Vector3 respawnCoordinates;

	private void OnTriggerEnter(Collider other)
	{
		if (!other.CompareTag("Player"))
			return;
		else
		{
			switch (coordinateSource)
			{
				default:
				case CoordinateSource.TransformPosition:
					CheckpointSystem.instance.SetRespawnPoint(this.transform.position);
					break;
				case CoordinateSource.ColliderPosition:
					CheckpointSystem.instance.SetRespawnPoint(transform.position + GetComponent<Collider>().bounds.center);
					break;
				case CoordinateSource.Vector3Variable:
					CheckpointSystem.instance.SetRespawnPoint(respawnCoordinates);
					break;
			}
		}
	}
}
