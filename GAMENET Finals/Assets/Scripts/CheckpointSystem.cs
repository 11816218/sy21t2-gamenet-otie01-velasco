using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointSystem : MonoBehaviour
{
	public static CheckpointSystem instance;

	private Vector3 respawnPoint = Vector3.zero;
	public void SetRespawnPoint(Vector3 newRespawnPoint) => respawnPoint = newRespawnPoint;
	public Vector3 GetRespawnPoint() => respawnPoint;
	public void RespawnPlayer(Transform respawnable) => respawnable.transform.SetPositionAndRotation(respawnPoint, Quaternion.identity);

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this);
		}
		else if (instance != this)
		{
			Destroy(this);
		}
	}
}
