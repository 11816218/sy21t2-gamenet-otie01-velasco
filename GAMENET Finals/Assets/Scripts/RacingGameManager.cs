using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class RacingGameManager : MonoBehaviourPunCallbacks
{
    public GameObject playerPrefab;
    public Transform[] startingPositions;
	public GameObject[] playerRankingTextUI;

	public List<GameObject> lapTriggers = new List<GameObject>();

    public static RacingGameManager instance = null;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
            Vector3 instantiatePosition = startingPositions[actorNumber - 1].position;
            PhotonNetwork.Instantiate(playerPrefab.name, instantiatePosition, Quaternion.identity);

            if (CheckpointSystem.instance)
            {
                CheckpointSystem.instance.SetRespawnPoint(startingPositions[actorNumber - 1].position);

            }
        }

		foreach (GameObject go in playerRankingTextUI)
		{
			go.SetActive(false);
		}
	}

}
